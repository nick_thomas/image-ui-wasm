use serde::Deserialize;
use yew::prelude::*;
mod imagelistprops;
use imagelistprops::ImageList;
use reqwasm::http::Request;

#[derive(Clone, PartialEq, Deserialize)]
pub struct ImageResponse {
    id: String,
    author: String,
    width: usize,
    height: usize,
    url: String,
    download_url: String,
}

#[function_component(App)]
fn app() -> Html {
    let images = use_state(|| vec![]);
    let value = use_state(|| 0u32);
    let dependency_value = value.clone();
    {
        let images = images.clone();
        let value = value.clone();
        use_effect_with_deps(
            move |_| {
                images.set(vec![]);
                let images = images.clone();
                let value = value.clone();
                wasm_bindgen_futures::spawn_local(async move {
                    let generate_url =
                        format!("https://picsum.photos/v2/list?page={}&limit=100", *value);
                    let generate_url_future = format!(
                        "https://picsum.photos/v2/list?page={}&limit=100",
                        *value + 1
                    );
                    // multiple fetch
                    let fetched_images: Vec<ImageResponse> = Request::get(&generate_url)
                        .send()
                        .await
                        .unwrap()
                        .json()
                        .await
                        .unwrap();
                    let fetched_images_future: Vec<ImageResponse> =
                        Request::get(&generate_url_future)
                            .send()
                            .await
                            .unwrap()
                            .json()
                            .await
                            .unwrap();
                    images.set(fetched_images);
                });
                || ()
            },
            dependency_value,
        );
    }
    //    let selected_image = use_state(|| None);
    // increment the value is not working
    let on_image_select = {
        let value = value.clone();
        Callback::from(move |_| value.set(*value + 1))
    };

    html! {
        <>
            <div class="center">
            <ImageList images={(*images).clone()} />
            <button onclick={on_image_select.clone()}>{"Generate Next Range"}</button>
            </div>
            </>
    }
}

fn main() {
    yew::Renderer::<App>::new().render();
}
