use yew::prelude::*;

use crate::ImageResponse;

const MAX_COUNT_OF_ITEMS: usize = 9usize;

fn get_random_buf() -> Result<[u8; 1024], getrandom::Error> {
    let mut buf = [0u8; 1024];
    getrandom::getrandom(&mut buf)?;
    Ok(buf)
}

#[derive(Clone, Properties, PartialEq)]
pub struct ImageListProps {
    pub images: Vec<ImageResponse>,
}
#[function_component(ImageList)]
pub fn image_list(ImageListProps { images }: &ImageListProps) -> Html {
    let random_number = get_random_buf().unwrap();
    let generate_items: Html = random_number
        .into_iter()
        .filter(|&id| {
            images
                .iter()
                .find(|item| item.id == id.to_string())
                .is_some()
        })
        .map(|id| {
            html! {
                <>
                    <img src={format!("https://picsum.photos/id/{}/200/200.webp",id)} alt="content"/>
                    </>
            }
        })
        .take(MAX_COUNT_OF_ITEMS)
        .collect();
    //   let generate_new_items = {
    //       let on_click = on_click.clone();
    //       let images = images.clone();
    //       Callback::from(move |_| {
    //           on_click.emit(images.clone());
    //       })
    //   };
    html! {<>
            <div class="imageHolder">
            {  generate_items}
            </div>
                <div class="split">
                <div class="bar"></div>
                </div>
    //            <button onclick={generate_new_items}>{"Generate"}</button>
                </>
        }
}
