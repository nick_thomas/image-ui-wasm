# Yew app for image gallery generator.

```
Install Rust using rustup(https://rustup.rs/)

Add toolchain for wasm
rustup target add wasm32-unknown-unknown

Install Trunk to bundle
cargo install --locked trunk

Serve
trunk serve

```
